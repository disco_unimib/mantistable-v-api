const mongoose = require('mongoose');
const host = process.env.MANTIS_HOST
const port = process.env.MONGO_MANTIS_PORT
const user = process.env.MONGO_MANTIS_USER
const pw = process.env.MONGO_MANTIS_PASSWORD
const dbname = process.env.MONGO_MANTIS_DBNAME

parameters = {
  'useNewUrlParser': true,
  'useFindAndModify': false,
  'useCreateIndex': true,
  'useUnifiedTopology': true
}
const conn = mongoose.createConnection(`mongodb://${user}:${pw}@${host}:${port}/${dbname}?authSource=admin`, parameters);

module.exports = conn;
