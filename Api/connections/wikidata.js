const mongoose = require('mongoose');

const host = process.env.MONGO_LAMAPI_HOST
const port = process.env.MONGO_LAMAPI_PORT
const user = process.env.MONGO_LAMAPI_USER
const pw = process.env.MONGO_LAMAPI_PASSWORD
const dbname = process.env.MONGO_LAMAPI_DBNAME

parameters = {
  'useNewUrlParser': true,
  'useFindAndModify': true,
  'useCreateIndex': true,
  'useUnifiedTopology': true
}
const conn = mongoose.createConnection(`mongodb://${user}:${pw}@${host}:${port}/${dbname}?authSource=admin`, parameters);

module.exports = conn;
