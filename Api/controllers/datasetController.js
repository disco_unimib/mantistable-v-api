let labels = require('../models/labels')
let dataset = require('../models/dataset')
let table = require('../models/table')
let row = require('../models/row')
let cea = require('../models/cea')
let cta = require('../models/cta')
let cpa = require('../models/cpa')
const axios = require('axios')
const mantisHost = process.env.MANTIS_HOST


const TOKEN = process.env.TOKEN
function validateToken(token) {
  if (token == TOKEN) {
    return true
  }
  return false
}

function inputValidation(token, params) {
  out = {"validate": true} 
  if (!validateToken(token)) {
    out["validate"] = false
    out["code"] = 401
    out["out"] = {"message": "Unauthorized"}
  }
  else {
    for(let param of params) {
      try {
        parseInt(param)
      }
      catch(Error) {
        out["validate"] = false
        out["code"] = 400
        out["out"] = {"message": "Invalid ID supplied"}
        break
      }
    }
  }
  return out
}


function validateResultFromQuery(error, result) {
  let out 
  if(error) {
    out = {"validate": false, "code": 400, "out":{"Error": error.toString()}}
  }
  else if (result == undefined || result.length == 0) {
    out = {"validate": false, "code": 404, "out":{"Error": "Not Found"}}
  }
  else {
    out = {"validate": true, "code": 200}
  }
  return out
}
 
async function getCtaData(idDataset, idTable) {
  let out =  {"cta": {}, "winningCandidates": {}}
  await cta.findOne({"idDataset": idDataset, "idTable": idTable}).then( (result, err) =>  {
    if (err) {out = { "error": err.toString() }; return}
    if (!result) {return}
    out = {
      "winningCandidates": result["winningCandidates"],
      "cta": result["cta"]
    }
  }).catch(function(err) { console.log(err)}) 
  return out
}

async function getCpaData(idDataset, idTable) {
  let out = {"cpa": {}, "winningCandidates": {}}
  await cpa.findOne({"idDataset": idDataset, "idTable": idTable}).then( (result, err) => {
    if (err) {out = { "error": err.toString() }; return}
    if (!result) {return}
    out = {
      "winningCandidates": result["winningCandidates"],
      "cpa": result["cpa"]
    }
  }).catch(function(err) { console.log(err);})   
  return out
}

async function getCeaData(idDataset, idTable) {
  let out = {}
  await cea.find({"idDataset": idDataset, "idTable": idTable}).then( (results, err) => {
    if (err) {out = [{ "error": err.toString() }]; return}
    if (!results) {return}
    for (let i=0; i<results.length; i++) {
      out[results[i]["row"]] = {
        "winningCandidates": results[i]["winningCandidates"],
        "cea": results[i]["cea"]
      }
    }
  }).catch(function(err) { console.log(err);})    
  return out
}

async function idResolution(ctaResults, cpaResults) {
  ctaResults["idResolution"] = {}
  cpaResults["idResolution"] = {}
  idToSolve = {"cta": new Set(), "cpa": new Set()}
  
  for(let idCol of Object.keys(ctaResults["cta"])) {
    idType = ctaResults["cta"][idCol]
    idToSolve["cta"].add(idType)
  }
  for(let idCol of Object.keys(cpaResults["cpa"])) {
    idPred = cpaResults["cpa"][idCol]
    idToSolve["cpa"].add(idPred)
  }
  finalIdToSolve = Array.from(idToSolve["cta"]).concat(Array.from(idToSolve["cpa"]))

  if (finalIdToSolve.length == 0) { return }

  await labels.find({"entity": {"$in": finalIdToSolve}}).then( (results, err) => {
    if (err) {out = [{ "error": err.toString() }]; return}
    if (!results) {out = []; return}
    for (let result of results) {
      idEntity = result["entity"]
      try {
        enLabel = result["labels"]["en"]
      } catch (error) {
        enLabel = ""
      }
      if (idToSolve["cta"].has(idEntity)) {
        ctaResults["idResolution"][idEntity] = enLabel
      }
      if (idToSolve["cpa"].has(idEntity)) {
        cpaResults["idResolution"][idEntity] = enLabel
      }
    }
  }).catch(function(err) { console.log(err);})    
  return 
}


function getContext() {
  context = {
    "wd": {
      "prefix": "wd:",
      "uri": "https://www.wikidata.org/entity/",
      "total": 0,
      "reconciliated": 0
    }
  }
  return context
}

function buildColumns(out, result, labelHeader, labelSubj, ctaResults, cpaResults, reconciliated) {
  totalToReconcile = result["data"].length
  
  for (let [i, label] of labelHeader.entries()) {
   
    if (result["metadata"] && result["metadata"][label]["tag"] == "SUBJ") {
      out["columns"][label]["role"] = "subject"
    }

    if (out["columns"][label]["kind"] && out["columns"][label]["kind"] == "entity") {
      out["columns"][label]["context"]["wd"]["total"] = totalToReconcile
      out["columns"][label]["context"]["wd"]["reconciliated"] = reconciliated[i]
    }

    //PUT cta data
    col = i.toString()
    
    if (col in ctaResults["cta"]) {
      idType = ctaResults["cta"][col]
      idTypeStr = `wd:${ctaResults["cta"][col]}`
      if (out["columns"][label]["metadata"].length == 0) {
        out["columns"][label]["metadata"].push({})
      }
      out["columns"][label]["metadata"][0]["type"] = [
        {
          "id": idTypeStr,
          "match": true,
          "name": ctaResults["idResolution"][idType],
          "score":ctaResults["winningCandidates"][col][idType]
        }
      ]
          
    }
    
    if (col in cpaResults["cpa"]) {
      idPred = cpaResults["cpa"][col]
      idPredStr = `wd:${cpaResults["cpa"][col]}`
      out["columns"][labelSubj]["metadata"][0]["property"].push(
        {
          "id": idPredStr,
          "obj": label,
          "match": true,
          "name": cpaResults["idResolution"][idPred],
          "score": cpaResults["winningCandidates"][col][idPred]
        }
      )
    }  
  }
}

function getCEAMetadata(winningEntities) {
  metadata = []
  entities = Array.isArray(winningEntities) ? winningEntities : Object.keys(winningEntities);
  for(let entity of entities) {
    id = Array.isArray(winningEntities) ? entity["id"] : entity;
    entity = Array.isArray(winningEntities) ? entity : winningEntities[entity];
    type = entity["types"].map(item => ({"id": `wd:${item.id}`, "name": `${item.name}`}))
    metadata.push({
      "id": `wd:${id}`,
      "name": entity["label"],
      "description": entity["description"],
      "type": type,
      "score": "score" in entity ? entity["score"] : entity["final score"],
      "match": false
    })
  }
  // check match true
  if (metadata.length == 1) {
    metadata[0]["match"] = true
  }
  return metadata
}

function buildRows(out, result, ceaResults) {
  reconciliated = []
  for (let i=0; i<result["nCols"]; i++) {
    reconciliated[i] = 0
  }
  for (let [i, row] of result["data"].entries()) {
    let ceaData = ceaResults[i]
    let idRow = `r${i}`
    out["rows"][idRow] = {
      "id": idRow,
      "cells": {}  
    }
    let idCol = 0
    for (const [key, value] of Object.entries(row)) {
      metadata = ceaData != undefined &&  result["metadata"][key]["tag"] != "LIT" ? getCEAMetadata(ceaData["winningCandidates"][idCol]) : []
      out["rows"][idRow]["cells"][key] = {
        "id": `${idRow}$${key}`,
        "label": value,
        "metadata": metadata
      }
      reconciliated[idCol] = reconciliated[idCol] + (metadata.length == 1 ? 1 : 0)
      idCol++
    }
  }
  return reconciliated
}

async function deleteDataset(idDataset) {
  result = await dataset.deleteMany({"id": idDataset})
  if (result["deletedCount"] == 0) {
    out = {"code": 404, "out":{"Error": "Not Found"}}
    return out
  }
  let collections = [table, row, cea, cpa, cta]
  for(let collection of collections) {
    await collection.deleteMany({"idDataset": idDataset})
  }
  return {"code": 200, "out":{"message": "successful operation"}}
}

async function deleteTable(idDataset, idTable) {
  result = await table.deleteMany({"id": idTable, "idDataset": idDataset})
  if (result["deletedCount"] == 0) {
    out = {"code": 404, "out":{"Error": "Not Found"}}
    return out
  }
  let collections = [row, cea, cpa, cta]
  for(let collection of collections) {
    await collection.deleteMany({"idDataset": idDataset, "idTable": idTable})
  }
  return {"code": 200, "out":{"message": "successful operation"}}
}

const datasetRoutes = (app, fs) => {

    app.get('/dataset', (req, res) => {
      dataset.find({}, function (err, results) {
        if (err) return res.status(400).json({"Error": err.toString()})
        res.json(results)
      })
    })

    app.get('/dataset/:idDataset', async(req, res) => {
      params = [req.params["idDataset"]]
      temp = inputValidation(req.headers["token"], params)
      
      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }

      result = await dataset.findOne({"id": req.params["idDataset"]}, {"_id":0})
      temp = validateResultFromQuery(false, result)

      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }
      res.status(temp["code"]).json(result)
      
    })

    app.get('/dataset/:idDataset/table', async (req, res) => {
      idDataset = req.params["idDataset"]
      params = [req.params["idDataset"]]
      temp = inputValidation(req.headers["token"], params)

      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }
      let results = await table.find({"idDataset": idDataset})
      temp = validateResultFromQuery(false, results)

      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }
      
      out = []
      for(let result of results) {
        out.push({
          "id": result["id"],
          "name": result["name"],
          "status": result["status"],
          "nRows": result["nRows"],
          "nCols": result["nCols"]
        })
      }
      res.status(temp["code"]).json(out)
      
    })

    app.get('/dataset/:idDataset/table/:idTable', async (req, res) => {
      idDataset = req.params["idDataset"]
      idTable = req.params["idTable"]

      params = [idDataset, idTable]
      temp = inputValidation(req.headers["token"], params)
      
      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }

      ctaResults = await getCtaData(idDataset, idTable)
      cpaResults = await getCpaData(idDataset, idTable)
      ceaResults = await getCeaData(idDataset, idTable)
      await idResolution(ctaResults, cpaResults)
      
      result = await table.findOne({"id": idTable, "idDataset": idDataset }, {"_id": 0})
      temp = validateResultFromQuery(false, result)

      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }
     
      nCells = result["nRows"] * result["nCols"]
      metadata = result["metadata"] == undefined ? {} : result["metadata"]
      nNECol = 0
      for(let col of Object.keys(metadata)) {
        if(metadata[col]["tag"] == "SUBJ" || metadata[col]["tag"] == "NE") {
          nNECol++
        }
      }
      nNECells = nNECol * result["nRows"]
      labelHeader = Object.keys(result["data"][0])
      mappingsType = {
        "SUBJ": "entity",
        "NE": "entity",
        "LIT": "literal",
      }
      let out = {
        "table": {
          "id": result["id"],
          "idDataset": result["idDataset"],
          "name": result["name"],
          "nCols": result["nCols"],
          "nRows": result["nRows"],
          "status": result["status"],
          "nCells": nCells,
          "nNECellsLinkable": nNECells,
          "metadata": metadata,
          "lastModifiedDate": result["lastModifiedDate"]
        },
        "columns": {},
        "rows": {}
      }


      // Build rows
      reconciliated = buildRows(out, result, ceaResults)
      
      tableMetadata = out["table"]["metadata"]

      labelSubj = null
      for (let [i, label] of labelHeader.entries()) {
        statusCea = reconciliated[i] > 0 ? "reconciliated" : "empty"
        out["columns"][label] = {
          "id": label,
          "label": label,
          "status": statusCea,
          "context": statusCea == "reconciliated" ? getContext() : {"wd": {}},
          "metadata": [],
          "kind": result["metadata"] != undefined ? mappingsType[result["metadata"][label]["tag"]] :  undefined // TODO check if this is Null
        }
        if (tableMetadata[label] && tableMetadata[label]["tag"] == "SUBJ") {
          labelSubj = label
          out["columns"][label]["metadata"][0] = {}
          out["columns"][label]["metadata"][0]["property"] = []
        } 
      }
    
      // Build columns
      buildColumns(out, result, labelHeader, labelSubj, ctaResults, cpaResults, reconciliated)
      columns = out["columns"]
      nCellsToReconcile = 0
      nCellsReconciliated = 0
      for(let col of Object.keys(columns)) {
        nCellsToReconcile += columns[col]["context"]["wd"]["total"] != undefined ? columns[col]["context"]["wd"]["total"] : 0
        nCellsReconciliated += columns[col]["context"]["wd"]["reconciliated"] != undefined ? columns[col]["context"]["wd"]["reconciliated"] : 0
      }
      out["table"]["nCellsToReconcile"] = nCellsToReconcile
      out["table"]["nCellsReconciliated"] = nCellsReconciliated
      res.json(out)
      
    })

    app.get('/dataset/:idDataset/annotation', (req, res) => {
      dataset.findOne({"id": req.params["idDataset"]}, {"_id": 0}, function (err, result) {
        if (err) return res.status(400).json({ "error": err.toString() })
        out = {"status": result["status"]}
        res.json(out)
      })  
    })

    app.get('/dataset/:idDataset/table/:idTable/annotation', (req, res) => {
      table.findOne({"id": req.params["idTable"], "idDataset": req.params["idDataset"]}, {"_id": 0}, function (err, result) {
        if (err) return res.status(400).json({ "error": err.toString() })
        out = {"status":result["status"]}
        res.json(out)
      })  
    })

    app.post('/dataset/:idDataset/', async(req, res) => {
      idDataset = req.params["idDataset"]
      params = [idDataset]
      temp = inputValidation(req.headers["token"], params)
      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }
      newName = req.body["name"]
      result = await dataset.findOneAndUpdate({"id": idDataset}, {"$set": {"name":newName}})
      temp = validateResultFromQuery(false, result)
      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }
      result["name"] = newName
      res.status(temp["code"]).json(result)
    })

    app.post('/dataset/:idDataset/annotation', async(req, res) => {
      let out
      auth = req.headers["token"]
      try {
        out = await axios.post(`http://${mantisHost}:5000/dataset/${req.params["idDataset"]}/annotation`, {}, {headers: {'auth': auth}});
        out = out.data
        console.log(out)
      }
      catch(error) {
        console.log(error)
        out = error
      }
      res.json(out)
    })

    app.post('/dataset/:idDataset/table/:idTable/annotation', async(req, res) => {
      let out
      auth = req.headers["token"]
      try {
        out = await axios.post(`http://${mantisHost}:5000/dataset/${req.params["idDataset"]}/table/${req.params["idTable"]}/annotation`, {}, {headers: {'auth': auth}});
        out = out.data
        console.log(out)
      }
      catch(error) {
        console.log(error)
        out = error
      }
      res.json(out)
    })

    app.delete('/dataset/:idDataset', async(req, res) => {
      idDataset = req.params["idDataset"]
      params = [idDataset]
      let temp = inputValidation(req.headers["token"], params)
      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }
      temp = await deleteDataset(idDataset)
      res.status(temp["code"]).json(temp["out"])
    })

    app.delete('/dataset/:idDataset/table/:idTable', async(req, res) => {
      idDataset = req.params["idDataset"]
      idTable = req.params["idTable"]
      params = [idDataset, idTable]
      let temp = inputValidation(req.headers["token"], params)
      if (!temp["validate"]) {
        res.status(temp["code"]).json(temp["out"])
        return
      }
      
      temp = await deleteTable(idDataset, idTable)
      res.status(temp["code"]).json(temp["out"])
    })


  }
  
module.exports = datasetRoutes
