const multer = require('multer')
const unzipper = require('unzipper')
const upload = multer()
const csv = require('csvtojson')

const table = require('../models/table')
const dataset = require('../models/dataset')
const { json } = require('body-parser')

const TOKEN = process.env.TOKEN
function validateToken(token) {
  if (token == TOKEN) {
    return true
  }
  return false
}


async function getDatasetId(datasetName) {
  let idDataset
  let result = await dataset.findOne({"name": datasetName})
  if (result == undefined) { 
    let result = await dataset.find({}).limit(1).sort({"$natural":-1})
    if (result.length == 0) { 
      idDataset = 0
    }
    else {
      result = result[0]
      idDataset = result["id"] + 1
    }
  }
  else {
    idDataset = result["id"]
  }
  return idDataset
}


async function checkExistDataset(idDataset) {
  datasetData = await dataset.findOne({"id": idDataset}, {"_id": 0})
  return datasetData
}


async function getTableId(idDataset) {
  let idTable
  let result = await table.find({"idDataset": idDataset}).limit(1).sort({"$natural":-1})
  if (result.length == 0) {
    idTable = 0
  }
  else {
    idTable = result[0]["id"] + 1
  }
  return idTable
}


async function updateDatasetStatistics(idDataset, datasetName, nAddTables) {
  let result = await table.aggregate([
      { "$match": { "idDataset": idDataset } },
      {
          "$group": {
            "_id":null,
            "nTables": {"$sum": 1}, 
            "avgRows": {"$avg":"$nRows"}, 
            "avgCols": {"$avg":"$nCols"},  
            "stdDevRows": {"$stdDevPop":"$nRows"}, 
            "stdDevCols": {"$stdDevPop":"$nCols"} 
          } 
      },
    ]
  )
  result = result[0]
  const update = {
    "$set": { 
      "name": datasetName,
      "nTables": result["nTables"],
      "nAvgRows": result["avgRows"].toFixed(1),
      "nAvgCols": result["avgCols"].toFixed(1),
      "stdDevRows": result["stdDevRows"].toFixed(1),
      "stdDevCols": result["stdDevCols"].toFixed(1)
    },
   "$inc": {"status.TODO": nAddTables, "status.DOING": 0, "status.DONE": 0}
  }
 
  await dataset.findOneAndUpdate({"id": idDataset}, update, {
    "upsert": true,
    "useFindAndModify": false
  });

  datasetData = await dataset.findOne({"id": idDataset}, {"_id": 0})
  return datasetData
}


async function addTable(idDataset, files) {
  let idTable = await getTableId(idDataset)
  buffer = []
  for(let file of files) {
    const content = await file.buffer();
    temp = content.toString();
    let data = await csv({
        noheader:false,
        output: "json"
    }).fromString(temp)
    let nCols = Object.keys(data[0]).length
    let nRows = data.length
    let name = file["path"].split(".")[0]
    temp = {
      "id": idTable,
      "idDataset": idDataset,
      "name": name,
      "data": data,
      "nCols": nCols,
      "nRows": nRows,
      "status": "TODO",
      "lastModifiedDate": new Date()
    }
    await table.create(temp)
    delete temp["data"]
    buffer.push(temp)
    idTable++
  }
  return buffer
}

const uploadRoutes = (app, fs) => {
  app.post("/dataset", upload.single("file"),
        async(req, res) => {
          datasetName = req.body["datasetName"]
          idDataset = await getDatasetId(datasetName)
          file = req.file["buffer"]
          const directory = await unzipper.Open.buffer(file);
          files = directory["files"]
          await addTable(idDataset, files)
          datasetData = await updateDatasetStatistics(idDataset, datasetName, files.length)
          res.statusCode = 202
          res.json(datasetData)
        }
  );

  app.post("/dataset/:idDataset/table", upload.single("file"),
        async (req, res) => {
          if (!validateToken(req.headers["token"])) {
            res.statusCode = 401
            res.json({"message": "Error Unauthorized"})
            return
          }
          idDataset = parseInt(req.params["idDataset"])
          datasetData = await checkExistDataset(idDataset)
          if (!datasetData) {
            res.statusCode = 404
            res.json({"message": "Error invalid idDataset"})
            return
          }
          datasetName = datasetData["name"]
          file = req.file
          buffer = file["buffer"]
          
          if (file["mimetype"] != "application/zip") {
            res.statusCode = 404
            res.json({"message": "Error invalid file format"})
            return
          }
          const directory = await unzipper.Open.buffer(buffer);
          files = directory["files"]
          buffer = await addTable(idDataset, files)
          datasetData = await updateDatasetStatistics(idDataset, datasetName, files.length)
          res.json(buffer)
        }
  );
}

module.exports = uploadRoutes
