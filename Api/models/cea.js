var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var conn = require('../connections/mantis')

var cea = new Schema({
  'idDataset': Number,
  'idTable': Number,
  'tableName': String,
  'row': Number,
  'winningCandidates': {},
  'cea': {}
},
  { versionKey: false }
)

module.exports = conn.model('cea', cea, 'cea') 
