var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var conn = require('../connections/mantis')

var cpa = new Schema({
  'idDataset': Number,
  'idTable': Number,
  'tableName': String,
  'row': Number,
  'winningCandidates': {},
  'cpa': {}
},
  { versionKey: false }
)

module.exports = conn.model('cpa', cpa, 'cpa') 
