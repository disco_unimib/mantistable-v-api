var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var conn = require('../connections/mantis')

var cta = new Schema({
  'idDataset': Number,
  'idTable': Number,
  'tableName': String,
  'row': Number,
  'winningCandidates': {},
  'cta': {}
},
  { versionKey: false }
)

module.exports = conn.model('cta', cta, 'cta') 
