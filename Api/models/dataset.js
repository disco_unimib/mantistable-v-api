var conn = require('../connections/mantis')
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dataset = new Schema({
    'id': Number,
    'name': String,
    'status': {},
    'nTables': Number,
    'nAvgCols': Number,
    'nAvgRows': Number,
    'stdDevCols': Number,
    'stdDevRows': Number
},
{versionKey: false }
)

module.exports = conn.model('dataset', dataset, 'dataset') 