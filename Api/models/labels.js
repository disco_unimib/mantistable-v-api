var mongoose = require('mongoose')
var Schema = mongoose.Schema
var conn = require('../connections/wikidata')

var labels =  new Schema({ // collection
  'id_entity': Number,
  'entity': String,
  'description': String,
  'labels': {},
  'aliases': {}
},
  { versionKey: false }
)

module.exports = conn.model('labels', labels, 'labels')