var conn = require('../connections/mantis')
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dataset = new Schema({
    'idDataset': Number,
    'idTable': Number,
    'tableName': String,
    'rows': Array,
    'batchSize': Number,
    'status': String
},
{versionKey: false }
)

module.exports = conn.model('row', dataset, 'row') 