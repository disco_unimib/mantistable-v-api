var conn = require('../connections/mantis')
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var table = new Schema({
    'id': Number,
    'idDataset': Number,
    'name': String,
    'data': [],
    'nCols': Number,
    'nRows': Number,
    'status': String,
    'metadata': {},
    'lastModifiedDate': Date
},
{ versionKey: false }
)

module.exports = conn.model('table', table, 'table') 