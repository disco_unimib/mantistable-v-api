// load up our shiny new route for users
const datasetRoutes = require('../controllers/datasetController')
const uploadRoutes = require("../controllers/uploadController")
const annotationRoutes = require("../controllers/annotationController")

const appRouter = (app, fs) => {
  // we've added in a default route here that handles empty routes
  // at the base API url
  app.get('/', (req, res) => {
    res.redirect("api-docs")
  })
  uploadRoutes(app, fs)
  // run our user route module here to complete the wire up
  datasetRoutes(app, fs)
  annotationRoutes(app, fs)
}

// this line is unchanged
module.exports = appRouter