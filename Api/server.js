// load up the express framework and body-parser helper
const express = require('express');
const bodyParser = require('body-parser');


// create an instance of express to serve our end points
const app = express();

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// we'll load up node's built in file system helper library here
// (we'll be using this later to serve our JSON files
const fs = require('fs');

// configure our express instance with some body-parser settings
// including handling JSON data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//setting middleware
app.use(express.static('static'))

// Pretty print if in development mode
if (process.env.NODE_ENV === "development") {
    app.set('json spaces', 4)
}


// Add headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    // TODO: Check
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    // TODO: Check
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,Accept, token, auth');
    // Include cookies for session...
    res.setHeader('Access-Control-Allow-Credentials', true);
    if (req.method === "OPTIONS") {
        return res.status(200).end();
    }
    // Pass to next layer of middleware
    next();
});

// this is where we'll handle our various routes from
const routes = require('./routes/routes.js')(app, fs);

// Constants
const PORT = process.env.MANTIS_API_PORT;
const HOST = '0.0.0.0';

// finally, launch our server on port specified.
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
