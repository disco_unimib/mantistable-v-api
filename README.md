# Citing

[1] Avogadro, R., & Cremaschi, M. (2021). MantisTable V: A novel and efficient approach to Semantic Table Interpretation. In SemTab@ ISWC (pp. 79-91).

Checkout s-Elbat [https://bitbucket.org/disco_unimib/selbat](https://bitbucket.org/disco_unimib/selbat)

# MantisTable API #
API to access MantisTable V approach

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
